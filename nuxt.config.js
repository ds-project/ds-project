module.exports = {
  /*
  ** Headers of the page
  */
  head: {
    title: 'Distributed file storage',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'Nuxt.js project' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ],
  },
  /*
  ** Customize the progress bar color
  */
  loading: { color: '#3B8070' },
  /*
  ** Build configuration
  */
  build: {
    /*
    ** Run ESLint on save
    */
    extend (config, { isDev, isClient }) {
      if (isDev && isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
    },
  },
  plugins: [
    '~/plugins/axios.js'
  ],
  modules: [
    '@nuxtjs/style-resources',
    '@nuxtjs/axios',
    ['nuxt-env', {
      keys: [
        {
          key: 'API_URL',
          default: process.env.API_ENDPOINT || 'http://localhost:5000/api'
        },
        {
          key: 'DATANODE_API_URL',
          default: process.env.DATANODE_API_URL || 'http://localhost:5005/api'
        }
      ]
    }],
  ],
  styleResources: {
    scss: [
      'assets/scss/main.scss',
    ]
  },
};

