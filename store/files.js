export const state = () => ({
  currentDirectory: 0,
  files: []
});

export const mutations = {
  setCurrentDirectory(state, directoryId) {
    state.currentDirectory = directoryId;
  },
  setFiles(state, files) {
    state.files = files;
  },
  addNewFile(state, newFile) {
    state.files.unshift(newFile);
  },
  removeItemFromFiles(state, file_id) {
    state.files.splice(state.files.findIndex(file => file.id === file_id), 1);
  },
  incrementSizeOfDirectory(state, folder_id) {
    let idOfDir = state.files.findIndex(file => file.id === folder_id);
    if (idOfDir !== -1) {
      state.files[idOfDir].size++;
    }
  },
  finishedUploading(state) {
    state.files = state.files.map(file => ({ ...file, uploading: false }))
  },
  startDownloading(state, fileId) {
    const fileIndex = state.files.findIndex(file => file.id === fileId);
    if (fileIndex > -1)
      state.files[fileIndex].downloading = true;
  },
  finishDownloading(state, fileId) {
    state.files = state.files.map(file => ({ ...file, downloading: false }))
  }
};
