import { saveAs } from 'file-saver';

export const state = () => ({
  chunkSize: 1024 * 1024,
  threadsQuantity: 5,
  file: null,
  error: "",
  aborted: false,
  completed: false,
  uploadedSize: 0,
  downloadedSize: 0,
  chunksQuantity: 0,
  chunksQueue: [],
  progressCache: {},
  activeConnections: {}
});

export const mutations = {
  setOptions(state, options = {}) {
    Object.keys(options).map((key) => state[key] = options[key]);
  },
  setupFile(state, file) {
    if (!file) return;
    state.completed = false;
    state.error = null;
    state.uploadedSize = 0;
    state.chunksQuantity = 0;
    state.chunksQueue = [];
    state.file = file;
  },
  throwError(state, error = "Something went wrong!") {
    state.error = error && error.data ? error.data : error;
  },
  popChunk(state) {
    state.chunksQueue.pop();
  },
  pushChunk(state, chunkId) {
    state.chunksQueue.push(chunkId);
  }
};


export const actions = {
  async start({ commit, state, dispatch }, { file_name, size, directory_id }) {
    if (!state.file) {
      return commit(
        "throwError",
        `Can't start uploading: file have not chosen`
      );
    }
    try {
      const response = await this.$axios.post("/upload_file", {
        file_name,
        size,
        directory_id
      });
      const { id, size_of_chunks, chunks, number_of_chunks, ...rest } = response.data;
      await commit("setOptions", {
        chunksQueue: chunks.map(chunk => chunk.id),
        fileId: id,
        chunksQuantity: number_of_chunks,
        chunkSize: size_of_chunks,
        error: null,
      });
      if (number_of_chunks === 0) {
        await commit('files/addNewFile', { id, uploading: false, size_of_chunks, chunks, ...rest }, { root: true });
        await commit("setOptions", {
          uploadedSize: 100,
          completed: true
        });
      } else await commit('files/addNewFile', { id, uploading: true, size_of_chunks, chunks, ...rest }, { root: true });
    } catch (e) {
      console.error(e);
      return commit("throwError", e.response);
    }
  },
  sendNext({ commit, state, dispatch }) {
    const activeConnections = Object.keys(state.activeConnections).length;

    if (activeConnections >= state.threadsQuantity) {
      return;
    }

    if (!state.chunksQueue.length) {
      if (!activeConnections) {
        commit("setOptions", { completed: true });
      }
      return;
    }

    const chunkId = state.chunksQueue[state.chunksQueue.length - 1];
    commit("popChunk");
    const sentSize = (state.chunksQuantity - state.chunksQueue.length - 1) * state.chunkSize;
    const chunk = state.file.slice(sentSize, sentSize + state.chunkSize);

    dispatch("sendChunk", { chunk, chunkId, fileName: state.file.name })
      .then(() => {
        dispatch("sendNext");
      })
      .catch(error => {
        commit("pushChunk", chunkId);
        console.error(error);
        commit("throwError", `${error}. Retrying...`);
        setTimeout(() => dispatch("sendNext"), 5000);
      });

    dispatch("sendNext");
  },
  sendChunk({ commit, state, dispatch }, { chunk, chunkId, fileName }) {
    return new Promise(async (resolve, reject) => {
      try {
        const formData = new FormData();
        formData.append('chunk', new File([chunk], fileName));
        formData.append('chunk_id', chunkId);
        const response = await fetch(`${this.$env.DATANODE_API_URL}/upload_chunk`, {
          method: 'post',
          body: formData
        });
        const { status } = response;
        const result = await response.json();
        if (status !== 200/* || size !== chunk.size*/) {
          reject(result && result.msg ? result.msg : 'Failed to upload chunk #' + chunkId);
          return;
        } else {
          const uploadedSize = Math.min(state.uploadedSize + state.chunkSize, state.file.size);
          commit("setOptions", { uploadedSize });
          console.log("Chunk uploaded!", { chunk, chunkId });
          if (uploadedSize === state.file.size)
            setTimeout(() => commit('files/finishedUploading', null, { root: true }), 1000)
        }
      } catch (error) {
        reject(error);
        return;
      }

      resolve();
    });
  },
  async downloadFile({ commit, state }, fileId) {
    try {
      const result = await this.$axios.get(`/download_file/${fileId}`);
      await commit('files/startDownloading', fileId, { root: true });
      commit("setOptions", { downloadedSize: 1 });
      const fileName = result.data.name;
      const chunkQueue = result.data.chunks.map(chunk => chunk.id).sort((a, b) => b.offset - a.offset);
      const chunkLength = chunkQueue.length - 1;
      const firstChunkId = chunkQueue[0];
      const downloadedChunks = Array(chunkQueue.length).fill(null);
      let activeConnections = 0;
      const downloadNextChunk = () => {
        if ((chunkQueue.length + activeConnections) === 0) {
          const downloadedFile = new File(downloadedChunks, fileName);
          setTimeout(() => {
            commit('files/finishDownloading', null, { root: true });
            commit("setOptions", { downloadedSize: 0 });
          }, 1000);
          saveAs(downloadedFile, fileName);
          return;
        }
        const chunkId = chunkQueue.pop();
        activeConnections++;
        if (activeConnections < 5)
          downloadNextChunk();
        if (chunkId)
          fetch(`${this.$env.DATANODE_API_URL}/download_chunk/${chunkId}`)
            .then(response => response.blob())
            .then((downloadedChunk) => {
              activeConnections--;
              downloadedChunks[chunkLength - (chunkId - firstChunkId)] = downloadedChunk;
              commit("setOptions", { downloadedSize: state.downloadedSize + downloadedChunk.size });

              console.log(`Chunk downloaded`, downloadedChunk);
              downloadNextChunk();
            }).catch((e) => {
              console.error(e);
              activeConnections--;
              chunkQueue.push(chunkId);
            });
        else activeConnections--;
      };
      downloadNextChunk();
    } catch (e) {
      commit("throwError", `Couldn't download the file`);
    }

  }
};
