# ds-project

> Nuxt.js project

## Build Setup

``` bash
# install dependencies
$ npm install # Or yarn install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm start

# generate static project
$ npm run generate
```

For detailed explanation on how things work, checkout the [Nuxt.js docs](https://github.com/nuxt/nuxt.js).

Open your browser  http://localhost:3000/

And you will see the main page.

## Architectural diagrams of the system

### Structure of the system
![](static/Diagrams/Structure.png)

### Uploading process
![](static/Diagrams/Upload_1.png)
![](static/Diagrams/Upload_2.png)
![](static/Diagrams/Upload_3.png)
![](static/Diagrams/Upload_4.png)
![](static/Diagrams/Upload_5.png)
![](static/Diagrams/Upload_6.png)

### Downloading process
![](static/Diagrams/Download_1.png)
![](static/Diagrams/Download_2.png)
![](static/Diagrams/Download_3.png)
![](static/Diagrams/Download_4.png)


## Communication protocols

Client-Naming Node - HTTPS

Client-Load Balancing - HTTPS

Load Balancing-Data Nodes - HTTP

Naming Node-Data Nodes - HTTP

